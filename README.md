# Tim's Website \[tims_flutter_website\]

![Build](https://img.shields.io/gitlab/pipeline/tim54000/tim.s-website.svg?gitlab_url=https%3A%2F%2Fframagit.org&style=flat-square)
[![visit_dev_site](https://img.shields.io/badge/visit-tim--dev.netlify.com-blueviolet.svg?style=flat-square)](https://tim-dev.netlify.com)
![website status](https://img.shields.io/website/https/tim-dev.netlify.com.svg?style=flat-square)
![website score on Mozilla HTTP Observatory](https://img.shields.io/mozilla-observatory/grade-score/tim-dev.netlify.com.svg?publish&style=flat-square)
![Tim's PGP key](https://img.shields.io/keybase/pgp/tim54000.svg?style=flat-square)

My new website based on Flutter for Web. Built since 2019.

## Table of Content

 * [About](#about)
 * [Getting started](#getting-started)
 * [Prerequisites](#prerequisites)
 * [Installing](#installing)
 * [Build using](#built-using)
 * [Authors](#authors)

## About

Here is the repo of my brand new website, currently under development. It is based on Flutter for Web,
an UI framework still in Technical preview. This site is intended to serve as a portfolio and also as
a directory of my projects in the same way as my Framagit account, while offering to display for some
projects the prices of the licenses associated with them and buy them.

## Getting started

These instructions will get you a copy of the project up and running on your local machine for development
and testing purposes.

### Prerequisites

You need to install the functional Flutter SDK with the Dart SDK. Since it is a website, you do not need 
the Android SDK.

To do so, follow the link : [Get started on Flutter.dev](https://flutter.dev/docs/get-started/install)

### Installing

First, clone the repository :  
with HTTP:
`git clone https://framagit.org/Tim54000/tim.s-website.git`  
or with SSH :
`git clone git@framagit.org:Tim54000/tim.s-website.git`

Get the dependencies:  
`flutter pub get`

Activate webdev ("the project dev server and builder"):  
`pub global enable webdev`

Launch the dev server:  
`webdev serve web:8080`

This last command serves the `web/` directory on the port `:8080`. To access the site, open the link: 
[http://localhost:8080](http://localhost:8080)


### Built using

 * Flutter for Web (UI Framework)
 * msgpack2 (to unwrap MessagePack data)
 * Netlify (to serve the website on the world)

### Author(s)

* @Tim54000 (website's owner)
