import 'package:flutter_web/cupertino.dart';
import 'package:flutter_web/material.dart';
import 'package:flutter_web/widgets.dart';
import 'package:tim_flutter_website/utils/uidata.dart';
import 'package:tim_flutter_website/utils/responsive_widget.dart';

import '../widgets/product_dialog.dart';

class ProductionsPage extends StatelessWidget {
  /* TODO:
   *    - Add a drawer
   *    - Create a class for "Production chips"
   *    - On tap on a Production : show a Dialog with description
   *    - Link Dialog to Paypal donations
   *    - Make productions load from "Data"
   */

  const ProductionsPage(this.data, {Key key}) : super(key: key);
  final Future<dynamic> data;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: UIData.blackBackground,
      body: ResponsiveWidget(
        largeScreen: CustomScrollView(
          slivers: <Widget>[
            SliverSafeArea(
              sliver: SliverAppBar(
                title: Text(
                  "Tim",
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                backgroundColor: UIData.appBarColor,
                floating: false,
                pinned: true,
                snap: false,
              ),
            ),
            SliverList(
              delegate: SliverChildListDelegate(
                <Widget>[
                  headerContainer(context),
                ],
              ),
            ),
            SliverPadding(
              padding: EdgeInsets.symmetric(
                  vertical: 80.0,
                  horizontal: MediaQuery.of(context).size.width > 1000
                      ? (MediaQuery.of(context).size.width - 1000) / 2
                      : 10.0),
              sliver: SliverGrid(
                gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                  maxCrossAxisExtent: 300,
                  mainAxisSpacing: 10.0,
                  crossAxisSpacing: 10.0,
                  childAspectRatio: 0.8,
                ),
                delegate: SliverChildBuilderDelegate(
                  (BuildContext context, int index) {
                    return Card(
                      child: Container(
                        decoration: BoxDecoration(
                          image: DecorationImage(
                            fit: BoxFit.cover,
                            image: NetworkImage(UIData.kotlinLogoSquare),
                          ),
                        ),
                        child: Material(
                          color: Colors.transparent,
                          child: InkWell(
                            onTap: (){
                              showProductDialog(context: context, ProductID: index);
                            },
                            child: Stack(
                              children: <Widget>[
                                Positioned(
                                  bottom: 0,
                                  left: 0,
                                  right: 0,
                                  child: Container(
                                      color: Colors.black54,
                                      child: Column(
                                        children: <Widget>[
                                          ListTile(
                                            title: Text("Project $index"),
                                            subtitle: Text("A example description..."),
                                            contentPadding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 2.0),
                                          ),
                                        ],
                                      ),
                                  ),
                                ),
                                Positioned(
                                  top: 5,
                                  right: 5,
                                  child: Chip(
                                    avatar: Icon(UIIcons.gitlab),
                                    label: Text(
                                      "CC-BY",
                                      style: TextStyle(fontWeight: FontWeight.w700),
                                    ),
                                    backgroundColor: Colors.blue,
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 2.0, horizontal: 5.0),
                                  ),
                                ),
                                Positioned(
                                  top: 5,
                                  left: 5,
                                  child: Tooltip(
                                    message: "Minecraft project",
                                    child: Chip(
                                      label: Icon(UIIcons.cube, size: 20.0),
                                      backgroundColor: Colors.green[400],
                                      padding: const EdgeInsets.symmetric(
                                          vertical: 2.0, horizontal: 5.0),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        )
                      ),
                    );
                  },
                  childCount: 20,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget headerContainer(BuildContext context) {
    return Container(
      color: Colors.white10,
      child: Column(
        children: <Widget>[
          Container(
            color: UIData.blackBackground,
            height: ResponsiveWidget.isLargeScreen(context)
                ? 300
                : ResponsiveWidget.isMediumScreen(context) ? 200 : 100,
            child: Center(
              child: Text(
                "Productions",
                maxLines: 1,
                style: Theme.of(context).textTheme.display2,
              ),
            ),
          ),
          ConstrainedBox(
            constraints: BoxConstraints(
              maxWidth: 1200.0,
            ),
            child: Center(
              child: Container(
                padding: const EdgeInsets.symmetric(
                    vertical: 40.0, horizontal: 20.0),
                child: Column(
                  children: <Widget>[
                    Text(
                      """
Sur cette page, vous retrouvez l'intégralité des productions que j'expose au public. La plus part d'entre-elles sont sous licenses Creative Commons ou équivalentes. Ces projets sont destinés à être utiliser par le plus de monde possible, gratuitement dès lors que leur usage n'est pas commerciale. Je trouve dommage de faire payer un service qui n'est utilisé qu'entre amis et non dans un but commercial déterminé; Nous voulons tous, et je le pense, un monde meilleur et cela commence par donner le savoir sans aucune contre-partie. Dans cette idée, j'espère que mes projets puissent contribuer à ce monde et peut-être inspiré d'autres personnes après moi.
               
Pour plus de visibilité, sont associées aux projets, des "chips", ce sont des pastilles de couleurs qui peuvent contenir un texte.

Si vous en croisez, voici leur signification : 

  * Chip noire => Ce projet est fermé aux acheteurs et l'intégralité des droits d'auteurs me sont réservés
  
  * Chip rouge incluant un prix => Ce projet n'est pas sous license libre et est achetable contre le prix indiqué.
  
  * Chip orange ( incluant parfois un prix ) => Ce projet est sous une license non-commerciale; le droit d'utilisation commerciale est potentiellement achetable contre le prix indiqué.
  
  * Chip bleue (+ icone) => Ce projet est sous license libre, l'utilisation quelqu'en soit l'usage est admis.
                        """,
                      style: TextStyle(
                        fontSize: ResponsiveWidget.isSmallScreen(context)
                            ? 14.0
                            : 16.0,
                      ),
                      textAlign: TextAlign.justify,
                    ),
                    Wrap(
                      crossAxisAlignment: WrapCrossAlignment.center,
                      spacing: 5.0,
                      runSpacing: 2.0,
                      children: <Widget>[
                        Text(
                          "Exemples :",
                          style: TextStyle(
                            fontSize: ResponsiveWidget.isSmallScreen(context)
                                ? 14.0
                                : 16.0,
                          ),
                        ),
                        Chip(
                          avatar: Icon(UIIcons.gitlab),
                          label: Text(
                            "CC-BY",
                            style: TextStyle(fontWeight: FontWeight.w700),
                          ),
                          backgroundColor: Colors.blue,
                          padding: const EdgeInsets.symmetric(
                              vertical: 2.0, horizontal: 5.0),
                        ),
                        Chip(
                          label: Text(
                            "10€",
                            style: TextStyle(fontWeight: FontWeight.w700),
                          ),
                          backgroundColor: Colors.deepOrange,
                          padding: const EdgeInsets.symmetric(
                              vertical: 2.0, horizontal: 5.0),
                        ),
                        Chip(
                          avatar: Icon(UIIcons.gitlab),
                          label: Text(
                            "HyTeam",
                            style: TextStyle(fontWeight: FontWeight.w700),
                          ),
                          backgroundColor: Colors.deepOrange,
                          padding: const EdgeInsets.symmetric(
                              vertical: 2.0, horizontal: 5.0),
                        ),
                        Chip(
                          label: Text(
                            "25€",
                            style: TextStyle(fontWeight: FontWeight.w700),
                          ),
                          backgroundColor: Colors.red,
                          padding: const EdgeInsets.symmetric(
                              vertical: 2.0, horizontal: 5.0),
                        ),
                        Chip(
                          avatar: Icon(Icons.copyright),
                          label: Text(
                            "Tim",
                            style: TextStyle(fontWeight: FontWeight.w700),
                          ),
                          backgroundColor: Colors.black,
                          padding: const EdgeInsets.symmetric(
                              vertical: 2.0, horizontal: 5.0),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
