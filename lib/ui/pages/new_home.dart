import 'package:flutter_web/animation.dart';
import 'package:flutter_web/material.dart';
import 'package:flutter_web/widgets.dart';

import 'package:tim_flutter_website/utils/responsive_widget.dart';
import 'package:tim_flutter_website/utils/uidata.dart';
import 'package:tim_flutter_website/ui/widgets/skill_card.dart';
import 'package:tim_flutter_website/ui/widgets/social_button.dart';
import 'package:tim_flutter_website/ui/widgets/circle_opener_project.dart';
import 'package:tim_flutter_website/ui/widgets/project_card.dart';

/// NewHomePage is the Home widget
///
/// required: Future data, the data unwrap from data.mp
class NewHomePage extends StatelessWidget {
  // TODO: Add a drawer =)
  // TODO: Group the Outline buttons (Header)
  // TODO: Make CircleOpenerProject load from the Data
  const NewHomePage(this.data, this.scroller, {Key key}) : assert(data != null && scroller != null), super(key: key);
  final Future<dynamic> data;
  final ScrollController scroller;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ResponsiveWidget(
        largeScreen: RefreshIndicator(
          onRefresh: () {
            return new Future.delayed(Duration(seconds: 2), () {
              // Not-implemented!
            });
          },
          child: CustomScrollView(
            controller: scroller,
            slivers: <Widget>[
              SliverSafeArea(
                sliver: SliverAppBar(
                  title: Text(
                    "Tim",
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  expandedHeight: MediaQuery.of(context).size.height,
                  flexibleSpace: headerContainer(context, scroller),
                  backgroundColor: UIData.appBarColor,
                  floating: false,
                  pinned: true,
                  snap: false,
                ),
              ),
              SliverList(
                delegate: SliverChildListDelegate(
                  <Widget>[
                    projectContainer(context),
                    Container(
                      color: UIData.blackBackground,
                      child: Center(
                        child: Container(
                          color: Colors.blueAccent,
                          height: 5.0,
                          width: MediaQuery.of(context).size.width / 2,
                        ),
                      ),
                    ),
                    skillContainer(context),
                    footerTile(context),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget headerContainer(BuildContext context, ScrollController scroller) {
    return FlexibleSpaceBar(
      collapseMode: CollapseMode.parallax,
      background: DecoratedBox(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [
              UIData.flexBarBackground,
              Colors.blueAccent,
              Colors.blue[600]
            ],
            begin: Alignment.topLeft,
            end: Alignment(0.8, 0.0),
            tileMode: TileMode.repeated,
          ),
        ),
        child: Padding(
          padding: const EdgeInsets.only(
              top: 56.0, right: 5.0, left: 5.0, bottom: 5.0),
          child: Center(
            child: FittedBox(
              fit: BoxFit.contain,
              alignment: Alignment.center,
              child: Column(
                children: <Widget>[
                  Card(
                    margin: const EdgeInsets.all(10.0),
                    clipBehavior: Clip.antiAlias,
                    elevation: 4.0,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0)),
                    //color: Colors.blueAccent,
                    color: Colors.transparent,
                    child: Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: Flex(
                        direction: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? Axis.vertical
                            : Axis.horizontal,
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          CircleAvatar(
                            radius: 100,
                            backgroundColor: Colors.transparent,
                            backgroundImage: NetworkImage(UIData.logoImg),
                          ),
                          SizedBox(width: 20.0, height: 20.0),
                          Column(
                            mainAxisSize: MainAxisSize.min,
                            crossAxisAlignment:
                                ResponsiveWidget.isSmallScreen(context)
                                    ? CrossAxisAlignment.center
                                    : CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: <Widget>[
                              Text(
                                "Tim54000",
                                style: TextStyle(
                                    fontWeight: FontWeight.w900,
                                    fontSize: 74.0),
                              ),
                              Text(
                                "Développeur Amateur",
                                style: TextStyle(
                                    fontWeight: FontWeight.w400,
                                    fontSize: 36.0),
                              ),
                              SizedBox(height: 20.0),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  Text(
                                    "~ In <3 with Rust",
                                    style: TextStyle(
                                        fontWeight: FontWeight.w300,
                                        fontSize: 20.0,
                                        color: Colors.white54),
                                  ),
                                  SizedBox(
                                    width: 125.0,
                                  ),
                                  SocialButton(
                                      "Gitlab",
                                      const Icon(
                                        UIIcons.gitlab,
                                        size: 30.0,
                                        color: Colors.white54,
                                      ),
                                      "https://framagit.org/Tim54000"),
                                  SizedBox(
                                    width: 10.0,
                                  ),
                                  SocialButton(
                                      "Twitter",
                                      const Icon(
                                        UIIcons.twitter,
                                        size: 30.0,
                                        color: Colors.white54,
                                      ),
                                      "https://twitter.com/MrTim54000"),
                                  SizedBox(
                                    width: 10.0,
                                  ),
                                  SocialButton(
                                      "Discord",
                                      const Icon(
                                        UIIcons.discord,
                                        size: 30.0,
                                        color: Colors.white54,
                                      ),
                                      "https://discordapp.com/"),
                                ],
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                  ButtonTheme.bar(
                    child: ButtonBar(
                      children: <Widget>[
                        OutlineButton(
                          onPressed: (){
                            scroller.animateTo(MediaQuery.of(context).size.height, duration: Duration(seconds: 2), curve: Curves.easeInOut);
                          },
                          child: Text("PROJETS", style: TextStyle(color: Colors.grey[200], fontSize: 20.0, fontWeight: FontWeight.w700),),
                          borderSide: BorderSide(
                              style: BorderStyle.solid,
                              color: Colors.grey[200],
                              width: 5.0
                          ),
                          padding: const EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
                        ),
                        OutlineButton(
                          onPressed: (){
                            scroller.animateTo(MediaQuery.of(context).size.height*3, duration: Duration(seconds: 2), curve: Curves.easeInOut);
                          },
                          child: Text("COMPETENCES", style: TextStyle(color: Colors.grey[200], fontSize: 20.0, fontWeight: FontWeight.w700),),
                          borderSide: BorderSide(
                              style: BorderStyle.solid,
                              color: Colors.grey[200],
                              width: 5.0
                          ),
                          padding: const EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
                        ),
                        OutlineButton(
                          onPressed: (){
                            Navigator.of(context).pushNamed(UIData.productionsRoute);
                          },
                          child: Text("PRODUCTIONS", style: TextStyle(color: Colors.grey[200], fontSize: 20.0, fontWeight: FontWeight.w700),),
                          borderSide: BorderSide(
                            style: BorderStyle.solid,
                            color: Colors.grey[200],
                            width: 5.0
                          ),
                          padding: const EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget projectContainer(BuildContext context) {
    return Container(
      color: UIData.blackBackground,
      padding: const EdgeInsets.symmetric(vertical: 80.0, horizontal: 10.0),
      child: Column(
        children: <Widget>[
          FittedBox(
            fit: BoxFit.scaleDown,
            alignment: Alignment.center,
            child: Container(
              padding: const EdgeInsets.only(bottom: 40.0),
              child: titleHeader(
                context,
                TextSpan(
                    text: " Mes Projets ",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20.0,
                    )),
              ),
            ),
          ), // Title - Header
          Center(
            child: ConstrainedBox(
              constraints: BoxConstraints(maxWidth: 1200.0),
              child: Flex(
                direction: ResponsiveWidget.isLargeScreen(context)
                    ? Axis.horizontal
                    : Axis.vertical,
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Flexible(
                    child: ProjectCard.fromData(data, 0),
                  ),
                  Flexible(
                    child: ProjectCard.fromData(data, 1),
                  ),
                ],
              ),
            ),
          ), // 2 Projects put forward
          SizedBox(height: 30.0),
          Center(
            child: Card(
              margin: const EdgeInsets.all(5.0),
              clipBehavior: Clip.antiAlias,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0)),
              color: Colors.black45,
              child: ConstrainedBox(
                constraints: BoxConstraints(maxWidth: 1200.0),
                child: Container(
                  padding: const EdgeInsets.all(20.0),
                  child: projects_to_show(context, data),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget projects_to_show(BuildContext context, Future<dynamic> data) {
    return FutureBuilder(
      future: data,
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          if (snapshot.hasData) {
            List<dynamic> copList = snapshot.data["projectCircleOpeners"];
            List<Widget> cops = <Widget>[];

            for (int i = 0; i < copList.length; i++) {
              cops.add(CircleOpenerProject.fromRaw(snapshot.data, i));
            }

            return Column(
              children: <Widget>[
                Container(
                  padding: const EdgeInsets.only(bottom: 20.0),
                  child: Text(
                    "~ A voir aussi ~",
                    style: TextStyle(fontSize: 20.0),
                  ),
                ),
                Wrap(
                  runAlignment: WrapAlignment.spaceAround,
                  crossAxisAlignment: WrapCrossAlignment.center,
                  spacing: 10.0,
                  runSpacing: 20.0,
                  children: cops,
                ),
              ],
            );
          }
          return Text(UIData.error);
        } else {
          return Text(UIData.loading);
        }
      },
    );
  }

  Widget skillContainer(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 80.0, horizontal: 10.0),
      color: UIData.blackBackground,
      child: Column(
        children: <Widget>[
          FittedBox(
            fit: BoxFit.scaleDown,
            alignment: Alignment.center,
            child: Container(
              padding: const EdgeInsets.only(bottom: 40.0),
              child: titleHeader(
                context,
                TextSpan(
                    text: " Mes Compétences ",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20.0,
                    )),
              ),
            ),
          ),
          Center(
            child: ConstrainedBox(
              constraints: BoxConstraints(maxWidth: 1200.0),
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.symmetric(
                        horizontal: ResponsiveWidget.isLargeScreen(context)
                            ? 80.0
                            : 20.0),
                    child: Text(
                      """Lorem ipsum... Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur? \n\n~ \"De Finibus Bonorum et Malorum\", Ciceron, 45 av. J.-C. ~""",
                      style: TextStyle(
                          fontSize: ResponsiveWidget.isSmallScreen(context)
                              ? 14.0
                              : 16.0),
                    ),
                  ),
                  SizedBox(height: 40.0),
                  Flex(
                    direction: ResponsiveWidget.isVeryLargeScreen(context)
                        ? Axis.horizontal
                        : Axis.vertical,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Flexible(
                        flex: 3,
                        child: SkillCard.fromData(data, 0),
                      ),
                      Flexible(
                        flex: 4,
                        child: SkillCard.fromData(data, 1),
                      ),
                      Flexible(
                        flex: 3,
                        child: SkillCard.fromData(data, 2),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget footerTile(BuildContext context) {
    return ListTile(
      title: Text("Tim's Website"),
      subtitle: Text("© 2019 - Tim54000 : All rights reserved"),
      isThreeLine: true,
      dense: true,
      onTap: () {
        showAboutDialog(
            context: context,
            applicationName: "Tim's Website",
            applicationVersion: "1.0.0",
            applicationIcon: Icon(
              Icons.cloud,
              color: Colors.white,
            ),
            applicationLegalese: "© 2019 - Tim54000 : All rights reserved");
      },
    );;
  }

  Text titleHeader(BuildContext context, TextSpan text) {
    return Text.rich(
      TextSpan(
          text: "",
          style: TextStyle(
            fontFamily: UIData.fontAwesome,
            fontSize: 18.0,
          ),
          children: <TextSpan>[
            text,
            TextSpan(
                text: "",
                style: TextStyle(
                  fontFamily: UIData.fontAwesome,
                  fontSize: 18.0,
                ))
          ]),
      maxLines: 1,
      style: TextStyle(
        color: Color(0xFFE1F5FE), //Colors.lightBlue[50],
      ),
    );
  }
}
