import 'package:flutter_web/material.dart';
import 'package:flutter_web/widgets.dart';
import 'dart:html' as html;
import 'package:tim_flutter_website/utils/uidata.dart';

class SkillCard extends StatelessWidget {
  const SkillCard(this.label,
      this.color,
      this.logo, {
        Key key,
        this.logoColor,
        this.headerColor,
        this.headerImage,
        this.projects,
      })
      : assert(label != null && color != null && projects != null),
        super(key: key);

  final Widget label;
  final Color color;
  final Color logoColor;
  final Color headerColor;
  final ImageProvider headerImage;
  final List<Widget> projects;
  final ImageProvider logo;


  factory SkillCard.fromRaw(Map<dynamic, dynamic> raw, int cardID) { // Build SkillCard on data.mp info
    List<dynamic> skillCardsList = raw["skillCards"];
    Map<dynamic, dynamic> skillCard = skillCardsList[cardID];
    List<dynamic> projects_raw = skillCard["projects"];
    List<Widget> projects = [];
    for (Map<dynamic, dynamic> project in projects_raw) {
      projects.add(ListTile(
        title: Text(project["name"]),
        subtitle: Text(project["description"]),
        trailing: project["on_tap"] != null ? Icon(Icons.arrow_forward) : null,
        onTap: project["on_tap"] != null ? (){
          html.window.open(project["on_tap"]["url"], project["on_tap"]["title"]);
        } : null,
      ));
    }
    return SkillCard(
      Text(skillCard["label"], style: TextStyle(fontSize: 20.0),),
      Color(skillCard["color"]),
      NetworkImage(skillCard["logo_url"]),
      logoColor: skillCard["logo_color"] != null ? Color(skillCard["logo_color"]) : null,
      headerColor: skillCard["header_color"] != null ? Color(skillCard["header_color"]) : null,
      projects: projects,
      headerImage: skillCard["header_image_url"] != null ? NetworkImage(skillCard["header_image_url"]) : null,
    );
  }

  static FutureBuilder fromData(Future<dynamic> data, int cardID) {
    return FutureBuilder<dynamic>(
      future: data,
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          if (snapshot.hasData) {
            return SkillCard.fromRaw(snapshot.data, cardID);
          }
          return SkillCard(
            Text(UIData.error, style: TextStyle(fontSize: 20.0),),
            Colors.red,
            NetworkImage(UIData.errorImage),
            projects: <Widget>[],
          );
        } else {
          return SkillCard(
            Text(UIData.loading, style: TextStyle(fontSize: 20.0),),
            Colors.deepOrange,
            NetworkImage(UIData.errorImage),
            projects: <Widget>[],
          );
        }
      },
    );;
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      color: color,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
      clipBehavior: Clip.antiAlias,
      child: Column(
        children: <Widget>[
          Container(
            decoration: headerImage != null
                ? BoxDecoration(
              image:
              DecorationImage(image: headerImage, fit: BoxFit.cover),
            )
                : null,
            color: headerColor ?? Colors.black26,
            padding: const EdgeInsets.all(20.0),
            child: Center(
              child: Column(
                children: <Widget>[
                  CircleAvatar(
                    radius: 50.0,
                    backgroundColor: logoColor ?? Colors.grey[300],
                    backgroundImage: logo,
                  ),
                  SizedBox(height: 10.0),
                  label,
                ],
              ),
            ),
          ),
          Column(
            children: projects,
          ),
        ],
      ),
    );
  }
}
