import 'package:flutter_web/material.dart';
import 'dart:html' as html;

class SocialButton extends StatelessWidget {
  const SocialButton(this.name, this.icon, this.url, {Key key})
      : assert(name != null && icon != null && url != null),
        super(key: key);
  final String name;
  final Icon icon;
  final String url;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        //Navigator.of(context).pushNamed(url);
        html.window.open(url, name);
      },
      child: icon,
    );
  }
}