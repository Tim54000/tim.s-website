import 'package:flutter_web/material.dart';

import '../../utils/uidata.dart';

class ProductDialog extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      content: SingleChildScrollView(
        child: ListBody(
          children: <Widget>[
            Row(
              children: <Widget>[
                if (MediaQuery.of(context).orientation == Orientation.landscape)
                  Image.network(
                    UIData.kotlinLogoSquare,
                    width: 48,
                    height: 48,
                  ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 24.0),
                    child: ListBody(
                      children: <Widget>[
                        Text("Project name",
                            style: Theme.of(context).textTheme.headline),
                        Text("version: 1.2.3",
                            style: Theme.of(context).textTheme.subtitle),
                        Text("licence: BY-NC-SA",
                            style: Theme.of(context).textTheme.subtitle),
                      ],
                    ),
                  ),
                ),
                if (MediaQuery.of(context).size.width > 800)
                  Wrap(
                    spacing: 5.0,
                    runSpacing: 5.0,
                  children: <Widget>[
                    Chip(
                      avatar: Icon(
                        UIIcons.link,
                        size: 15.0,
                      ),
                      label: Text(
                        "GITLAB",
                        style: TextStyle(fontWeight: FontWeight.w700),
                      ),
                      backgroundColor: Colors.blue,
                      padding: const EdgeInsets.symmetric(
                          vertical: 2.0, horizontal: 5.0),
                    ),
                    Chip(
                      avatar: Icon(UIIcons.link, size: 15.0),
                      label: Text(
                        "HYTALE.community",
                        style: TextStyle(fontWeight: FontWeight.w700),
                      ),
                      backgroundColor: Colors.blue,
                      padding: const EdgeInsets.symmetric(
                          vertical: 2.0, horizontal: 5.0),
                    ),
                    Chip(
                      avatar: Icon(UIIcons.gitlab),
                      label: Text(
                        "CC-BY",
                        style: TextStyle(fontWeight: FontWeight.w700),
                      ),
                      backgroundColor: Colors.blue,
                      padding: const EdgeInsets.symmetric(
                          vertical: 2.0, horizontal: 5.0),
                    ),
                  ],
                ),
              ],
            ),
            SizedBox(
              height: 20.0,
            ),
            Container(
              constraints: BoxConstraints(
                maxWidth: 1200.0,
              ),
              child: Text(
                """Lorem ipsum... Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur? \n\n~ \"De Finibus Bonorum et Malorum\", Ciceron, 45 av. J.-C. ~""",
                style: Theme.of(context).textTheme.body1,
              ),
            ),
          ],
        ),
      ),
      actions: <Widget>[
        FlatButton(
          child: Text('OBTENIR'),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        FlatButton(
          child: Text('FERMER'),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
      ],
    );
  }
}

void showProductDialog({
  @required BuildContext context,
  @required int ProductID,
}) {
  assert(context != null && ProductID != null);
  showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return ProductDialog();
      });
}
