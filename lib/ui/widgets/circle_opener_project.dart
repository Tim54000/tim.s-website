import 'package:flutter_web/material.dart';
import 'dart:html' as html;

import '../../utils/uidata.dart';

class CircleOpenerProject extends StatelessWidget {
  const CircleOpenerProject(this.name, {Key key, this.color, this.image, this.onTap}) : assert(name != null), super(key: key);

  final String name;
  final Color color;
  final ImageProvider image;
  final VoidCallback onTap;


  factory CircleOpenerProject.fromRaw(Map<dynamic, dynamic> raw, int id) {
    List<dynamic> copList = raw["projectCircleOpeners"];
    Map<dynamic, dynamic> cop = copList[id];
    return CircleOpenerProject(
      cop["name"],
      color: cop["color"] != null ? Color(cop["color"]) : null,
      image: cop["image_url"] != null ? NetworkImage(cop["image_url"]) : null,
      onTap: cop["about"] != null ? () {
        html.window.open(
          cop["about"]["url"],
          cop["about"]["title"],);
      } : null,
    );
  }

  static FutureBuilder fromData(Future<dynamic> data, int id) {
    return FutureBuilder<dynamic>(
      future: data,
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          if (snapshot.hasData) {
            return CircleOpenerProject.fromRaw(snapshot.data, id);
          }
          return CircleOpenerProject(
           UIData.error,
            color: Colors.redAccent,
          );
        } else {
          return CircleOpenerProject(
            UIData.loading,
            color: Colors.transparent,
          );
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Tooltip(
      message: name,
      child: CircleAvatar(
        radius: 40.0,
        backgroundImage: image,
        backgroundColor: color ?? Color(4292927712),
        child: InkWell(
          radius: 30.0,
          splashColor: Colors.red,
          onTap: onTap,
        ),
      ),
    );
  }
}
