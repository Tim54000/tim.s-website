import 'package:flutter_web/material.dart';
import 'package:tim_flutter_website/utils/uidata.dart';
import 'dart:html' as html;




class ProjectCard extends StatelessWidget {
  const ProjectCard(this.name, this.description, this.image_url,
      {Key key, this.icon, this.get, this.about})
      : assert(name != null && description != null),
        super(key: key);

  factory ProjectCard.fromRaw(Map<dynamic, dynamic> raw, int cardID) { // Build ProjectCard on data.mp info
    List<dynamic> projectCardsList = raw["projectCards"];
    Map<dynamic, dynamic> projectCard = projectCardsList[cardID];
    return ProjectCard(
      Text(projectCard["name"]),
      Text(projectCard["description"]),
      projectCard["image_url"],
      icon: projectCard["icon"] != null ? Icon(IconData(projectCard["icon"], fontFamily: UIData.fontAwesome)) : null,
      about: projectCard["about"] != null ? (){
        html.window.open(
            projectCard["about"]["url"],
          projectCard["about"]["title"],);
      } : null,
      get: projectCard["get"] != null ? (){
        html.window.open(
          projectCard["get"]["url"],
          projectCard["get"]["title"],);
      } : null,
    );
  }

  static FutureBuilder fromData(Future<dynamic> data, int cardID) {
    return FutureBuilder<dynamic>(
      future: data,
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          if (snapshot.hasData) {
            return ProjectCard.fromRaw(snapshot.data, cardID);
          }
          return ProjectCard(
            Text(UIData.error),
            Text(UIData.serverError),
            null,
            icon: Icon(Icons.error),
          );
        } else {
          return ProjectCard(
            Text(UIData.tooFast),
            Text(UIData.loading),
            null,
            icon: Icon(Icons.cloud_download),
          );
        }
      },
    );
  }

  final Icon icon;
  final Widget name;
  final Widget description;
  final String image_url;
  final VoidCallback get;
  final VoidCallback about;

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: const EdgeInsets.all(5.0),
      clipBehavior: Clip.antiAlias,
      color: Colors.black54,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
      child: Column(
        children: <Widget>[
          AspectRatio(
            aspectRatio: 2,
            child: Image(
              fit: BoxFit.cover,
              image: NetworkImage(image_url ?? UIData.projectCardExempleImage),
            ),
          ),
          ListTile(
            leading: icon ?? const Icon(UIIcons.gitlab),
            title: name,
            subtitle: description,
            //isThreeLine: true,
            //dense: true,
          ),
          ButtonTheme.bar(
            child: ButtonBar(
              children: <Widget>[
                FlatButton(
                  child: Text("OBTENIR"),
                  onPressed: get,
                ),
                FlatButton(
                  child: Text("EN SAVOIR PLUS"),
                  onPressed: about,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}