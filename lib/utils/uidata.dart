import 'dart:core';

import 'package:flutter_web/material.dart';
import 'package:flutter_web_ui/ui.dart';

class UIData {
  //routes
  static const String homeRoute = "/home";
  static const String productionsRoute = "/productions";
  static const String notFoundRoute = "/No Found";

  //strings
  static const String appName = "Tim's Website";

  //fonts
  static const String googleFont = "GoogleSansRegular";
  static const String fontAwesome = "FontAwesome";

  //images
  static const String assetDir = "assets";
  static const String imageDir = "$assetDir/images";
  static const String logoImg = "$imageDir/favicon-196.png";

  @deprecated
  static const String flutterLogoSquare = "$imageDir/flutter-logo.png";
  @deprecated
  static const String rustLogoSquare = "$imageDir/rust-lang-logo.svg";
  @deprecated
  static const String kotlinLogoSquare = "$imageDir/kotlin-logo.png";
  static const String projectCardExempleImage = "$imageDir/project-card-exemple.png";
  static const String errorImage = "$imageDir/error.svg";
  // static const String exampleImage = "$imageDir/example.webp"

  //data-files
  static const String dataMSGPACK = "/data.mp";

  //colors
  static const Color black75 = Color(0xCC000000);
  static const Color blackBackground = Color(0xFF0A0A0A);
  static const Color appBarColor = Color(0xFF64B5F6); // Colors.blue[300];
  static const Color flexBarBackground = Colors.lightBlue;

  //generic
  static const String error = "Erreur";
  static const String success = "Succès";
  static const String tooFast = "Trop rapide...";
  static const String loading = "Chargement en cours...";
  static const String networkError = "Une erreur réseau s'est produite !";
  static const String serverError = "Le serveur a produit une erreur...";
  static const String ok = "OK";
}

class UIIcons {
  static const gitlab = IconData(0xf296, fontFamily: UIData.fontAwesome);
  static const twitter = IconData(0xf099, fontFamily: UIData.fontAwesome);
  static const discord = IconData(0xf392, fontFamily: UIData.fontAwesome);
  static const exclamation = IconData(0xf071, fontFamily: UIData.fontAwesome);
  static const external_ling_alt = IconData(0xf35d, fontFamily: UIData.fontAwesome);
  static const link = IconData(0xf0c1, fontFamily: UIData.fontAwesome);
  static const cube = IconData(0xf1b2, fontFamily: UIData.fontAwesome);
}