import 'dart:typed_data';
import 'package:http/http.dart' as http;
import 'package:msgpack2/msgpack2.dart';

class DataFetcher {
  // TODO: Make a Data Class to regroup these data in a typed object.
  const DataFetcher(this.base_url);

  final base_url;

  Future<Uint8List> fetch() async {
    final res = await http.get(base_url);
    if (res.statusCode != 200)
      throw "HTTP Error, fetch data failed!";
    return res.bodyBytes;
  }

  dynamic parse(Uint8List data) {
    return deserialize(data);
  }

  Future<dynamic> fetch_and_parse() async{
    final bytes = await fetch();
    return parse(bytes);
  }
}