import 'package:flutter_web/material.dart';
import 'package:flutter_web/widgets.dart';

import 'ui/pages/production_page.dart';
import 'ui/pages/new_home.dart';
import 'utils/data_fetcher.dart';
import 'utils/uidata.dart';
import 'dart:html' as html;

class MyApp extends StatelessWidget {

  static final Future<dynamic> data = DataFetcher(UIData.dataMSGPACK).fetch_and_parse();

  final materialApp = MaterialApp(
    title: UIData.appName,
    theme: ThemeData(
      brightness: Brightness.dark,
      primaryColor: Color(0xff212121),
      primaryColorLight: Color(0xff484848),
      primaryColorDark: Color(0xff000000),
      accentColor: Colors.lightBlueAccent,
    ),
    debugShowCheckedModeBanner: true,
    showPerformanceOverlay: true,
    home: NewHomePage(data, ScrollController(initialScrollOffset: 0.0)),
    routes: <String, WidgetBuilder>{
      UIData.homeRoute: (BuildContext context) => NewHomePage(data, ScrollController(initialScrollOffset: 0.0)),
      UIData.productionsRoute: (BuildContext context) => ProductionsPage(data),
    }
  );

  @override
  Widget build(BuildContext context) {
    data.then((raw) {
      html.window.console.log("Data is loaded! $raw");
    });
    return materialApp;
  }
}